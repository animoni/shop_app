require 'test_helper'

class SignupUserTest < ActionDispatch::IntegrationTest
    
    test 'succesful signup' do
        get new_user_path
        assert_template 'users/new'
        assert_select 'form'
        assert_difference 'User.count', 1 do
            post users_path, params: { user: { username: "test_user"}}
        end
        follow_redirect!
        assert_template 'users/show'
        assert_select 'h3', text: 'username: test_user'
    end
    
    test 'unsuccesful signup' do
        get new_user_path
        assert_template 'users/new'
        assert_select 'form'
        assert_no_difference 'User.count' do
            post users_path, params: { user: { username: ""}}
        end
        assert_template 'users/new'
        
        
    end
    
end