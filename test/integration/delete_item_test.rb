require 'test_helper'

class DeleteItemTest < ActionDispatch::IntegrationTest
    
    def setup
        @item = Item.create(name: "Test", price: 12)
    end
    
    test 'the saved item is deleted from the database' do
        get items_path
        assert_template 'items/index'
        assert_select 'a[href=?]', item_path(@item), text: 'Delete'
        
        assert_difference 'Item.count', -1 do
            delete item_path(@item)
        end
        follow_redirect!
        assert_template 'items/index'
    
    end
end
