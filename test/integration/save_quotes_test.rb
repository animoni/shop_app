require 'test_helper'

class SaveQuotesTest < ActionDispatch::IntegrationTest
  test 'when not logged in it redirects to the signup page' do
    get quotes_path
    assert_select 'h3', text: "Search quotes" 
    post quotes_path, params: {favourite_quote: Quotes.last.content}
    assert_redirected_to new_user_path
  end
end