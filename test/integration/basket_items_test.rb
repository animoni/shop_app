require 'test_helper'

class BasketItemsTest < ActionDispatch::IntegrationTest
   
   def setup
      @item = Item.create(name: "Pad", price: 12) 
   end
    
    test 'it adds an item to the basket' do
      get items_path
      assert_select 'a[href=?]', basket_items_path(item_id: @item.id), text: 'Add to Basket'
      post basket_items_path(item_id: @item.id)
      get basket_items_path
      assert_select "td", "Pad"
    end 
    
    test  'deletes the whole basket' do
        get basket_path
        
    end
    
end