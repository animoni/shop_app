require 'test_helper'

class CreateBookmarkTest < ActionDispatch::IntegrationTest
  
  test 'when not loged in it redirects to the signup page' do
    get items_path
    assert_select 'a', text: "Bookmark" 
    post bookmarks_path, params: {item_id: Item.last.id}
    assert_redirected_to new_user_path
  end
  
  test 'when loged in it adds to the bookmark' do
    user = User.create(username: 'sheila')
    post login_path, params: {session: {username: 'sheila'}}
    get items_path
    assert_select 'a', text: "Bookmark" 
    post bookmarks_path, params: {item_id: Item.last.id}
    assert_redirected_to bookmarks_path
    follow_redirect!
    assert_select 'td', text: Item.last.name
  end
  
end