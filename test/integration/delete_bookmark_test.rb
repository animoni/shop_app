require 'test_helper'

class DeleteBookmarkTest < ActionDispatch::IntegrationTest
    
  test 'bookmark should be deleted if the current user is logged in' do
    user = User.create(username: 'sheila')
    post login_path, params: {session: {username: 'sheila'}}
    get bookmarks_path
    assert_select 'a[href= ?]', bookmarks_path
    assert_difference 'Bookmark.count',
  end
    
    
end