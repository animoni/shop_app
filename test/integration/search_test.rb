require 'test_helper'

class SearchTest < ActionDispatch::IntegrationTest

  test 'search with empty string returns all the item' do
    get search_items_path, params: {search_params: {name: ""}}
    #binding.pry
    assert_select 'td', count: 3
    
  end

  test 'search with a string and it should find item' do
    get search_items_path, params: {search_params: {name: "MyInteger"}}
    #binding.pry
    assert_select 'td', count: 1
    assert_select 'div.alert', text: "Couldn't find item.Sorry:(", count: 0
  end
  
  test 'search for something that it is not there, tell it to the user' do
    get search_items_path, params: {search_params: {name: "MyFloat"}}
    
    follow_redirect!
    #binding.pry
    assert_select 'td', text: "MyInteger"
    assert_select 'div.alert', text: "Couldn't find item.Sorry:("
  end
  
  test 'search term sholud be case insensitive' do
    get search_items_path, params: {search_params: {name: "myinteger"}}
     
    assert_select 'td', count: 1, text: "MyInteger"
  end
  
  test 'search word should return all items that are similar to it' do
    get search_items_path, params: {search_params: {name: "my"}}
     
    assert_select 'td', count: 2 
  end
  
  test 'search range should give back the items in between' do
    get search_items_path, params: {search_params: {name: "", min: 2, max: 6}}
    
    assert_select 'td', count: 3 
  end
  
  test 'empty search should give back all the items' do
    get search_items_path, params: {search_params: {name: "", min: "", max: ""}}
     
    assert_select 'td', count: 3
  end
  
   
  test 'search should give back the specified item' do
    get search_items_path, params: {search_params: {name: "my", min: 1, max: 6}}
     
    #assert_select 'td', count: 1, text: "MyString"
    assert_select 'td', count: 1, text: "MyInteger"
  end
  
  test 'search should give back the searched term' do
    get search_items_path, params: {search_params: {name: "my", min:"" , max: ""}}
     
    assert_select 'td', count: 2 
  end
  
  test 'search should give back the searched term with name and max value' do
    get search_items_path, params: {search_params: {name: "fl", min:"" , max: 6 }}
     
    assert_select 'td', count: 1, text: "Float"
  end
end