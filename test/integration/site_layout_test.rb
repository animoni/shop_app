'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
    test 'test search on the static page' do
        get root_path
        assert_template 'static_pages/home'
        assert_select 'a[href=?]', root_path
        assert_select 'a[href=?]', login_path
    end
end