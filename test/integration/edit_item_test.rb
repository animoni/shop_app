require 'test_helper'

class EditItemTest < ActionDispatch::IntegrationTest
    
    def setup 
       @item = Item.create(name: "Test", price: 12) 
    end
    
    test 'item can not be edited due to wrong parameters' do
        get edit_item_path(@item)
        assert_template 'items/edit'
        patch item_path(@item), params: {item: {name:  ""}}
        assert_template 'items/edit'
        assert_select 'div.alert', "Invalid information."
        
    end
    
end