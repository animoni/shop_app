require 'test_helper'

class CreateNewItemTest < ActionDispatch::IntegrationTest
  
  test "invalid new item creation for the new item" do
    get items_path
    assert_template 'items/index'
    get new_item_path
    assert_template 'items/new'
    assert_no_difference 'Item.count' do
      post items_path, params: {item: {name: "", price: ""}}
    end
    assert_template 'items/new'
    assert_select "#error_explanation"
    
  end
  
  test "valid new item creation" do
    get items_path
    assert_template 'items/index'
    get new_item_path
    assert_template 'items/new'
    assert_difference 'Item.count', 1 do
      post items_path, params: {item: {name: "test", price: 12}}
    end
        follow_redirect!
    assert_template 'items/show'
    assert_select 'a[href=?]', items_path
  end

end
