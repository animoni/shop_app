require 'test_helper'

class ItemTest < ActiveSupport::TestCase
    
  def setup
      @item = Item.new(name: "Product", price: 12, hidden: "", add_to_basket: "")
      @item_two = items(:item_two)
      @item_one = items(:item_one)
  end
    
  test 'item should be valid' do
    assert @item.valid?
  end
  
  test 'item should not be valid' do
    @item.price = 'foo'
    assert_not @item.valid?
  end
  
  test 'item\'s price should not be 0' do
    @item.price = 0
    assert_not @item.valid?
  end
  
  test 'item\'s price should not be negative number' do
    @item.price = -12
    assert_not @item.valid?
  end
  
  test 'item should be saved with valid information ' do 
    @item.save!
    assert @item.valid?
  end
  
  test 'item should not be saved with blank name' do
    item = Item.new(name:"", price: 12, hidden: "")
    item.save
    assert_not item.valid?
  end
  
  #test the search function
  
  test 'it should return that item which has all three attributes' do
    search_params = {name: "my", min: 1, max: 3}
    item = Item.search_all_parameters(search_params)
    result = item[0][:name]

    assert_includes(result, @item_one[:name])
    assert_equal 1, item.count
  end
  
  test 'it should return the item where the price is between 4 and 5' do
    search_params = {name: "", min: 4, max: 5}
    item = Item.min_max(search_params)
    result = item[0][:name]
    
    assert_includes(result, @item_two[:name])
    assert_equal 1, item.count
  end
  
   test 'it should return that item which name is similar to the searched term' do
    search_params = {name: "fl", min: "", max: ""}
    item = Item.name_search(search_params)
    
    assert_equal 1, item.count
  end
  
  test 'it should return all items which have their price under than 3' do
    search_params = {name: "", min: "", max: 3}
    item = Item.max_margin(search_params)
    
    assert_equal 1, item.count
  end
  
  test 'it should return all items which have their price over than 3' do
    search_params = {name: "", min: 4, max: ""}
    item = Item.min_margin(search_params)
    
    assert_equal 2, item.count
  end
  
  test 'it should return items with the similar name and max price' do
    search_params = {name: "my", min: "", max: 3}
    item = Item.max_margin(search_params)
    
    assert_equal 1, item.count
  end
  
  test 'it should return items with the similar name and min price' do
    search_params = {name: "my", min: 5, max: ""}
    item = Item.min_margin(search_params)
    
    assert_equal 1, item.count
  end
  
  
end
