class AddAddToBasketToItems < ActiveRecord::Migration[5.1]
  def change
    add_column :items, :add_to_basket, :boolean, default: false
  end
end
