class CreateFavouriteQuotes < ActiveRecord::Migration[5.1]
  def change
    create_table :favourite_quotes do |t|
      t.text :quote
      t.references :user

      t.timestamps
    end
  end
end
