class Quote

  def self.search(movie)
    filter = MovieQuotes.new
    if movie =~ /\A\d+\z/
      filter.by_year("#{movie}").results
    else
      filter.by_multiple("#{movie}").results
    end
    
  end
  
end