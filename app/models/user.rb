class User < ApplicationRecord
    has_many :item
    validates_uniqueness_of :username
    validates :username, presence: true, length: { maximum: 25 }
end