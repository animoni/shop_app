class Item < ApplicationRecord

  validates :name, presence: true, length: { maximum: 50 }
  validates :price, presence: true
  validate :integer?
  
  def integer?
      unless price.to_i.positive?
          errors.add(:price, "should be a valid number")
      end
  end
  
  def self.visible
      where(hidden: false)
  end
  
 
  # def self.search_all_parameters(search_params)
  #   Item.where('name LIKE ? AND price BETWEEN ? AND ?',"%#{search_params[:name].downcase}%" ,search_params[:min],search_params[:max])
  # end
   #Everything is given for the search
  scope :search_all_parameters, ->(search_params) {where('name LIKE ? AND price BETWEEN ? AND ?',"%#{search_params[:name].downcase}%" ,search_params[:min],search_params[:max]) }

  scope :min_max, ->(search_params) {where('price BETWEEN ? AND ?',search_params[:min],search_params[:max])}

  scope :name_search, ->(search_params) {where('name LIKE ?', "%#{search_params[:name].downcase}%")}
  
  scope :max_margin, ->(search_params) {where('name LIKE ? AND price <= ?',"%#{search_params[:name].downcase}%", search_params[:max])}
  
  scope :min_margin, ->(search_params) {where('name LIKE ? AND price >= ?',"%#{search_params[:name].downcase}%", search_params[:min])}
  



  def perform_search(search_params)
   # binding.pry
        if all_present?
          Item.search_all_parameters(search_params)
          
        elsif min_max_values_present?
          Item.min_max(search_params)

        elsif name_value_present?
          Item.name_search(search_params)
          
        elsif name_max_values_present?
          Item.max_margin(search_params)
          
        elsif name_min_values_present?
          Item.min_margin(search_params)
        else
          
        end
  end
  
      def name_value_present?
        search_params[:name].present? && !search_params[:max].present? && !search_params[:min].present?
      end
      
      def all_present?
        search_params[:name].present? && search_params[:max].present? && search_params[:min].present?
      end

      def min_max_values_present?
       search_params[:min].present? && search_params[:max].present? && !search_params[:name].present?
      end
        
      def name_max_values_present?
        search_params[:name].present? && search_params[:max].present?
      end
      
      def name_min_values_present?
        search_params[:name].present? && search_params[:min].present?
      end
        
        
end