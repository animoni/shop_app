class FavouriteQuote < ApplicationRecord
    belongs_to :user
    
    def price_for_item
        alphabet = ('a'..'z').to_a
        number = (1..26).to_a.reverse
        Hash[alphabet.zip(number)]
    end
    
    
    def price
        choosen = self.quote.downcase.split(//)
        price = price_for_item
    
        total = []
        choosen.each do |q|
           if price.has_key? (q)
              total << price.fetch(q) 
           end
        end
            sprintf('%0.2f', total.reduce(:+)/50)
    end
    
end
