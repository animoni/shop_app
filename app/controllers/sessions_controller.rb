class SessionsController < ApplicationController
  def new
    @session_form = Session.new
   
  end

  def create
    # @item = Item.new(item_params)
    # params.require(:item).permit(:name, :price, :hidden)
    @session_form = Session.new(session_params)
    
    if @session_form.valid?
      # login
      # redirect to user page
      user = User.find_by(username: @session_form.username)
      log_in(user)
      redirect_to user
    else
      render 'new'
    end
  end
  
  def destroy
    log_out
    redirect_to root_url
  end
  
  private
  
  def session_params
    params.require(:session).permit(:username)
  end
  
end
 #@session = Session.new(User.find_by(username: params[:session][:username]))

