class UsersController < ApplicationController
    
    def new
      @user = User.new
    end
    
    def create
      @user = User.new(user_param)
      if @user.save
        flash[:success] = "Yey, you signed up."
        log_in(@user)
        redirect_to @user
      else
        render 'new'
      end
    end
    
    def index
     
    end
    
    def show
       @user = User.find(params[:id])
      # @items = @user.items 
    end
    
   
    
  private

    def user_param
      params.require(:user).permit(:username)
    end
    
end
