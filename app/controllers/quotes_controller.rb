class QuotesController < ApplicationController


   
  def index
    @quotes = Quote.search(params[:search])
  end
  
  def create 
      if current_user.present?
        #binding.pry
        FavouriteQuote.create(quote: params[:favourite_quote], user_id: current_user.id)
        redirect_to bookmarks_path
      else
         redirect_to new_user_path
      end
  end
  
  def destroy
    @favourite_quote = FavouriteQuote.find(params[:id])
    @favourite_quote.destroy
    redirect_to bookmarks_path
  end
  
  private
  

end