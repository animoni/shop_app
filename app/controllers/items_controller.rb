class ItemsController < ApplicationController
    def index
        @items = Item.visible
        
    end
    
    def new
        @item = Item.new
    end
    
    def search
      #binding.pry
      @items = if none_present?
        Item.all
      else
        perform_search(search_params)
      end
      empty_item
    end
    
    def create
        @item = Item.new(item_params)
        if @item.save
            redirect_to @item
           # flash[:success] = "New item created."
        else
           # flash[:danger] = "Name can't be blank"
            render 'new'
        end
    end
    
    def show
       @item = find_item
    end
    
    def edit 
        @item = find_item
    end
    
    def update
        @item = find_item
        if @item.update_attributes(item_params)
            @item.save#it can update individually the params
            flash.now[:info] = "Item updated."
            redirect_to @item
        else
            flash.now[:danger] = "Invalid information."
            render 'edit'
        end
    end

    def destroy 
       @item = find_item 
       @item.delete
       redirect_to root_path
    end
    
    def hide
        @item = find_item
        @item.destroy
        redirect_to items_url
    end


    private
    
      def item_params
          params.require(:item).permit(:name, :price, :hidden, :add_to_basket)
      end
      
      def search_params
         params.require(:search_params).permit(:name, :min, :max) 
      end
      
      def find_item
         Item.visible.find(params[:id] ) 
      end
      
      def empty_item
        if @items.blank?
          flash[:warning] = "Couldn't find item.Sorry:("
          redirect_to items_path 
        end
      end
            
      def none_present?
        !search_params[:name].present? && !search_params[:max].present? && !search_params[:min].present?
      end

        
end