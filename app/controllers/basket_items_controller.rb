class BasketItemsController < ApplicationController
    
  def index
    if session[:items_in_basket].nil?
      flash[:danger] = "Basket is empty."
      redirect_to basket_items_path
    else
      @basket_items = Item.find(session[:items_in_basket])
      @basket_items ||= Array.new
    end
    
   
    
  end
    
  def create
    item_id = params[:item_id]
    @item = Item.find(item_id)
    @items_in_basket = session[:items_in_basket]
    @items_in_basket ||= Array.new
    @items_in_basket.push(@item.id)
    session[:items_in_basket] = @items_in_basket
    flash[:info] = "Item added to basket."
    
  end
    
  def destroy 
    #reset_session
    #item.delete
   
    # session[:items_in_basket].find(params[:basket_item])
    #session[:items_in_basket].delete(params[:id])
    # binding.pry
  
    session[:items_in_basket].delete_if{|x| x == params[:basket_item].to_i}
  #  session.delete(basket_item.id)
    flash[:danger] = "Item deleted."
    redirect_to basket_items_path
    flash[:danger] = "Basket is empty." if session[:items_in_basket].blank?
  end
    
  
    
end
