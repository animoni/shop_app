class BookmarksController < ApplicationController
      include BookmarksHelper
      
    def create
      if current_user.present?
        item_id = params[:item_id]
        @item = Item.find(item_id)
        @bookmark = Bookmark.create(user_id: current_user.id, item_id: @item.id)
  
        flash[:notice] = "Item bookmarked"
        redirect_to bookmarks_path
      else
        redirect_to new_user_path
      end
            
    end
    
    def index
      @bookmarks = Bookmark.where(user_id: current_user)
      @favourite_quote = FavouriteQuote.where(user_id: current_user)
    end
    
  def destroy
    @bookmark = Bookmark.find(params[:id])
    #binding.pry
    @bookmark.destroy
    redirect_to bookmarks_path
  end
    
  private
    
 
    
end
