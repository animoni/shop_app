class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  
    def current_user
      return nil if session[:user_id].nil?
      User.find(session[:user_id])
    end
end
