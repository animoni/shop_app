Rails.application.routes.draw do
  get 'sessions/new'

   root 'static_pages#home'
   resources :items do
    collection do
     get 'search'
    end
   end
   
   resources :users
    get    '/login',   to: 'sessions#new'
    post   '/login',   to: 'sessions#create'
    delete '/logout',  to: 'sessions#destroy'

   resources :basket_items, only: [:create, :index, :destroy] 
   
   get '/basket', to: 'basket_items#index'
   
   resources :quotes, only: [:index, :create, :destroy]
   
   resources :bookmarks
 
 #  get '/search', to: 'search#run'
end
